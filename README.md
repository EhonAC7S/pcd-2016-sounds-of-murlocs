# MurlocSoundLikeMaster

## Collaborateurs

CESARO Jordan, 
RICHARD Axel, 
SOCHALA Antoine, 
WANG Philippe.

# Démo de l'application

https://www.youtube.com/watch?v=3kR4zjxODs4

# Application Executable

L'essentiel de l'application executable est un *.jar* contenant notre code source. Il sera zippé avec ses dépendances comme les librairies maven projet associées et les librairies externes listées ci-dessous, ses images et la base de données. 
-> Sur Windows : un double-clic suffit pour lancer l'application
-> Sur les distributions linux : il faut faire *java* *-jar* *.jar*

## Problème de release finale

-> Nous avions une version fonctionnelle mais qui n'a pas été taggé, sans les boutons graphiques rouges, et dont le *.zip* n'a pas été fait

-> Nous avons une version fonctionnelle avec les améliorations graphiques des boutons, mais nous avons été victime du temps lors des differents push et des merges, ce qui fait que le *.zip* et *.jar* finaux ont été compréssé après 15h (date/heure du rendu attendu)

Nous soumettons donc en dernier push, dans la release en retard les deux versions énoncées ci dessus par honnêteté. Nous n'avons pas modifier de code après 15h, nous avons seulement fait les fusions des codes.

# Projet dans Eclipse

## Dépendances 

Environnement Java :  JavaSE-1.6
 
APIs :

+ API Youtube v3 (installée via Maven Project)
+ API *vget* par *axet* pour télécharger une video en deux fichiers séparés son et video.
*pom.xml* modifié pour importer toutes les librairies Maven nécessaires.

Bibliothèques : 
 
+ DJNativeSwing (/lib), il faut faire "Add To BuildPath"
+ DJNativeSwing-SWT (/lib), il faut faire "Add To BuildPath"
+ JNA (/lib), il faut faire "Add To BuildPath"
+ SWT, toutes versions et OS (/lib/swt) Elle est automatiquement choisie

Base de Données : SQLite 

## Prérequis 

-> sur distribution linux/ubuntu libwebkitgtk est requis : *sudo apt-get install libwebkitgtk-1.0-0*

-> Flash player si non déja installé.

Deux fichiers de paramétrages non fournis par ce dépot :

### youtube.properties 

Il faut le placer dans le dossier *src/ids/*.
ce fichier contient votre *apikey*.
	
### client_secret.json

Fichier de paramétrage présent dans */src/ids/* et contenant votre *client_id* et votre *client_secret*.


## Fonctionnalités

### LUNDI

-> Faire une recherche à partir de mots clés, gestion en console. Cela est géré par RequeteYoutube.java pour le moment.

-> Affichage uniquement d'une vidéo youtube, à partir de son URL/ID dans une fenêtre, il s'agit de YoutubeViewer.java, et représente une première alternative pour gérer cet affichage.

### MARDI

-> Mise en commun des fonctionnalités de LUNDI par la mise en place d'une interface graphique permettant la lecture d'une vidéo, soit par son ID déjà connu.

-> Mise en place du *.jar* executable executant la fonctionnalité précédante mais en dehors d'eclipse (notre IDE de développement).

-> L'instanciation de la base de données peut être fait à partir des éléments travaillés LUNDI. Nous avons choisi SQLite.

EDIT : L'application contourne les publicités classiques de youtube.

### MERCREDI

-> Interface graphique fait une recherche par mots clés en ligne, affichage des images, titres et auteurs.

-> La séléction d'une vidéo dans l'espace de recherche parmet la lecture de celle ci pas l'action d'un bouton play.

-> Nous avons géré la compatibilté Windows/Linux (MacOS non testé).

-> Mise en place de la recherche en local parmi les favoris enregistrés dans la BD, possibilité de trouver une vidéo favorie par son titre, id, url,... Les Videos sont triées par nombre de vues (pertinence par défaut) 

-> Mise en place de l'ajout de favoris, mais non relié à l'interface graphique. Les vidéos sont des objets VOD.

EDIT : Des petits problèmes ont été rencontrés par la suite au niveau cross plateform.
EDIT : Ajout de l'icone pour l'application.

### JEUDI

-> Compréhension de l'API pour télécharger les vidéos, parfois la bande son et la bande video sont dans deux fichiers *.mp4* séparés. Nous avons une fonction qui permet le téléchargement par l'ID, elle créé des dossiers pour le tri, et stock les URL locales des fichiers. Nous n'aurons surement pas le temps de trouver une façon de merger les deux fichiers. Mais nous pouvons proposer la fonctionnalité seule du téléchargement sans pouvoir jouer ces *.mp4*.

-> Liaison de la BD à l'interface pour l'ajout des favoris. Les favoris sont maintenant ajoutés par un bouton de l'interface.

-> Création de playlist temporaire par séléction multiple lors des recherches.

-> Multiples améliorations visulles. (icones et image boutons)

-> Correction de multiples bugs.

-> Mise en place d'un jar standalone et cross plateform.

-> Bouton *Télécharger* fonctionnel, mais bloque l'interface le temps du téléchargement. (Il faut mettre en place un thread) (hors jar executable)

EDIT : le jar n'est pas standalone.

### Vendredi

-> Threads pour le téléchargement de videos

-> correction de bugs sur les apostrophes.

-> Bouton pour retirer des favoris.

-> Recherche dans les favoris, avec plusieurs mots clés (espaces permis)

-> Image de  lancement de l'application.

-> Multiples amélioration graphique.

-> Navigation dans le dossier des téléchargements de l'application.

-> Mise en place de l'application finale de notre projet. Contenant un *.jar dans un dossier contenant les dépendances suivantes : /img/ ; /lib/ ; /MurlocSoundMaster_lib/ ; une base de données.

## Fonctionnalités à venir et prévision


-> Téléchargement de vidéos : Nous n'aurons surement pas le temps de permmettre leur lecture après téléchargement, mais l'utilisateur pourra la récupérer dans le dossier (DL/) (Nous n'avons pas eu le temps de mettre en place une lecture interne à l'application des videos téléchargées, mais l'utilisateur peut les récupérer et les lire avec une application externe comme VLC.)

-> Affichage d'informations supplémentaires par rapport aux videos : commentaires, description, etc... (Nous en avons ajouté pas pas tout)

La plannification prévisionnelle est faite dans un .pdf dans */documentation* et les jalons réellement accompli seront listés ci-dessus par jour.


## Remarques et problèmes rencontrés

-> Le choix de la bibliothèque de DJNative nous a forcé l'utilisation de SWT qui est une bibliothèque scpécifique à chaque OS et selon 32 ou 64 bits. Cela nous pose donc des problèmes de portativité entre Windows et Linux essentiellement. Nous avons pu contourné le choix de la bibilothèque grace à la classe *YoutubeViewInit.java* qui détermine le *.jar* à intégrer au buildpath. Cependant nous continuons d'avoir des problèmes d'execution dont nous ne connaissons pas l'origine sur la distribution ubuntu. Les machines Windows quant à elles font fonctionner le projet sans difficultés. (réglé, installation de *JNA* et *libwebkitgtk*)

-> La complexité d'utilisation d'une base de données MySQL nous a fait choisir SQLite, qui nous semble mieux adapté et plus simple d'utilisation/implémentation.

-> L'intégration des images dites *aperçu* des vidéos lors de la recherche (résolu) et le non-centrage et le multi-ligne du texte dans la *Jlist* de la recherche Youtube. (résolu)

-> L'API vget a été intégré au *pom.xml*, mais pas encore utilisée, elle permet d'obtenir une bande son et vidéo séparées, nous cherchons un moyen pour "merger" les deux pour le moment. (résolu)

-> Des erreurs/warnings nous sont renvoyés par l'API DJNative, mais l'application fonctionne tout de même, nous ne connaissons pas l'origine de ces warnings.

-> Problème d'execution du jar executable si les libs n'était pas présentes dans le même dossier parent. (résolu) L'application est donc Standalone.



## Ebauche de diagramme de class UML

Voici un diagramme présentant la première approche de conception. Elle sera ammenée à évoluer.


![UML class diagram](http://www.plantuml.com/plantuml/png/ZL6x3i8m3Dpp5HwBK7u0LGmGmOW4KYN6qhhKKjM0cx00_quI-e2MWI6AVNS-EqvMfKq36wpHC6uvlPHqjGXtCRzfoY4j2ifAJ2X77IKxDHDF8R9GiqSdD3XAZzeQUme641mONJsx4nqCKtsUEgoK-alksZkT14DkL5Z0M-20t39wW6hZCsmofa-Ur93rRgvh5wvrRLXNo4Bq8SHI1iqtCbxKYEvsqjuGFmQmtSnFpZVgS2cN_gbYyYSjyG80)


