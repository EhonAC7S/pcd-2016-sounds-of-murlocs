package codingweek2016;

import java.util.ArrayList;

public class ContainerSearch {

	long id;
	String req;
	ArrayList<String> tags = new ArrayList<String>();
	int nbRes;
	
	public void getTags() {
		String[] decoupe = this.req.replace("'", "").replace(".", "").replace(",", "").replace("?", "").replace("!", "").split(" ");
		for (String myString : decoupe) {
			tags.add(myString);
			}
		}

	public void setText(String recherche) {
		req = recherche;

	}
	
	public int getSize(ArrayList<String> tags) {
		return tags.size();
	}
	
	public String getValue(int i) {
		return tags.get(i);
	}
	
}
