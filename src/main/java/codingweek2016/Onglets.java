package codingweek2016;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

//objectif:renvoyer un jpanel qui contient nos 2 onglets de "recherche" suivant le mode
//ajouter à notre jframe par un JFrame.getContentPane().add(new Onglets(ongletA,ongletB),BorderLayout.NORTH);
// ex : frame.getContentPane().add(new Onglets(new JPanel(),new JPanel()), BorderLayout.NORTH);

public class Onglets extends JPanel {
	
	private JPanel ongletA;
	private JPanel ongletB;
	
	public Onglets(JPanel ongletA,JPanel ongletB){
		this.ongletA=ongletA;
		this.ongletB=ongletB;
		JTabbedPane volets = new JTabbedPane(SwingConstants.TOP);
		this.ongletA.setPreferredSize(new Dimension(700, 10));//réflechir aux tailles nécessaires pour faire joli
		volets.addTab("online", this.ongletA);
		volets.addTab("offline", this.ongletB);
		volets.setOpaque(true);
		this.add(volets);
		
	}

}
