package codingweek2016;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.common.collect.Lists;

/**
 * permet de faire une recherche de videos avec mot(s)-clé et nombre de
 * résultats voulus affiche l'url de la video et son titre et son auteur (nom de
 * la chaine)
 */
public class RequeteYoutube {

	private static final String PROPERTIES_FILENAME = "youtube.properties";// nom
																			// du
																			// fichier
																			// avec
																			// la
																			// clé
																			// id
	private static final long NUMBER_OF_VIDEOS_RETURNED = 25; // nombre par
																// défaut de
																// videos
	private static YouTube youtube;// on instancie youtube^^(pour faire nos
									// recherches)

	public static void main(String[] args) {
		// Lis la clé de développeur du fichier de propriétés.
		Properties proprietes = new Properties();

		try {
			InputStream in = RequeteYoutube.class.getResourceAsStream("/ids/" + PROPERTIES_FILENAME);
			proprietes.load(in);

		} catch (IOException e) {
			System.err.println("Erreur de lecture en lisant : " + PROPERTIES_FILENAME + ": " + e.getCause() + " : "
					+ e.getMessage());
			System.exit(1);
		}

		// This OAuth 2.0 access scope allows for full read/write access to the
		// authenticated user's account.
		List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");

		try {

			// Authorize the request.
			Credential credential = Auth.authorize(scopes, "updatevideo");

			// initialisation de l'objet youtube pour les recherches
			/*
			 * youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT,
			 * Auth.JSON_FACTORY, new HttpRequestInitializer() { public void
			 * initialize(HttpRequest request) throws IOException { }
			 * }).setApplicationName("Sounds of Murloc").build();
			 */

			youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
					.setApplicationName("Sounds of Murloc").build();

			String motCles = getMotCles();// demande les mots clés de recherche
											// à l'utilisateur
			Long nbRes = getNbRes(); // demande nombre de resultats

			YouTube.Search.List recherche = youtube.search().list("id,snippet");// cree
																				// la
																				// liste
																				// de
																				// resultats
																				// de
																				// recherche

			String cleApi = proprietes.getProperty("youtube.apikey");
			recherche.setKey(cleApi);
			recherche.setQ(motCles);

			// restreindre aux videos
			recherche.setType("video");

			// recuperation des champs utiles ensuite seulement
			// snippet/thumbnails/default/url : si veut thumbnail pour plus tard
			// contentDetails/duration : durée video
			// snippet/channelTitle : titre chaine
			// snippet/tags : liste de tags
			// recherche.setFields("items(id/kind,id/videoId,snippet/title,snippet/channelTitle");//inutilisé
			// maintenant par autre methode après
			recherche.setMaxResults(nbRes);

			// affiche resultats
			SearchListResponse resultats = recherche.execute();
			List<SearchResult> resultatsListe = resultats.getItems();
			if (resultatsListe != null) {
				affichageConsole(resultatsListe.iterator(), motCles, nbRes);
			}
		} catch (GoogleJsonResponseException e) {
			System.err.println("Erreur de service : " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
		} catch (IOException e) {
			System.err.println("Erreur IO : " + e.getCause() + " : " + e.getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private static long getNbRes() throws IOException {

		String NbResString = "";

		System.out.print("Combien voulez vous de résultats: (25 par défaut) ");
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		NbResString = buffer.readLine();

		if (NbResString.length() < 1) {
			// 25 par defaut
			return NUMBER_OF_VIDEOS_RETURNED;
		}
		return Long.parseLong(NbResString);
	}

	private static void affichageConsole(Iterator<SearchResult> iteratorResultatsListe, String motCles, Long nbRes) {

		System.out.println("\n");
		System.out.println("	Affichage des " + nbRes + " premiers résultats de recherche pour \"" + motCles + "\".");
		System.out.println("\n");

		if (!iteratorResultatsListe.hasNext()) {
			System.out.println("	Il n'y a aucun résultat pour votre recherche.");
		}

		while (iteratorResultatsListe.hasNext()) {
			// List<Video> testlist=(List<Video>) iteratorResultatsListe;
			SearchResult video = iteratorResultatsListe.next();
			// Video test = testlist.get(0);
			ResourceId rId = video.getId();

			YouTube.Videos.List listVideosRequest;
			try {
				// rajouter les champs voulus dans list("")
				listVideosRequest = youtube.videos().list("snippet,contentDetails,statistics").setId(rId.getVideoId());
				VideoListResponse listResponse = listVideosRequest.execute();
				List<Video> videoList = listResponse.getItems();
				Video test = videoList.get(0);

				// verification que c'est bien une video contenue
				if (rId.getKind().equals("youtube#video")) {

					// Thumbnail thumbnail =
					// video.getSnippet().getThumbnails().getDefault();
					// thumbnail.getUrl() //utile pour la bdd après
					String duree = test.getContentDetails().getDuration();

					System.out.println("URL : https://www.youtube.com/watch?v=" + rId.getVideoId());
					// System.out.println("URL Video Seule :
					// https://www.youtube.com/embed/" + rId.getVideoId() );
					System.out.println("Titre : " + video.getSnippet().getTitle());
					System.out.println("Auteur : " + video.getSnippet().getChannelTitle());
					System.out.println("Liste des tags : " + test.getSnippet().getTags());
					System.out.println("Statistiques : " + test.getStatistics().getLikeCount() + " pouces bleus");
					System.out.println("Durée : " + duree.substring(2, duree.length()));
					System.out.println("Lien image : " + test.getSnippet().getThumbnails().getDefault().getUrl());
					System.out.println("\n");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static String getMotCles() throws IOException {

		String motCles = "";

		System.out.print("Veuillez entrer votre recherche : ( \"murloc\" par défaut ) ");
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		motCles = buffer.readLine();

		if (motCles.length() < 1) {
			// murloc par défaut
			motCles = "murloc";
		}
		return motCles;
	}
}
