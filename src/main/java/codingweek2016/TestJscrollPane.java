package codingweek2016;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
 
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
 
public class TestJscrollPane {
 
    public static void main(String[] args) {
 
        SwingUtilities.invokeLater(new Runnable() {
 
            @Override
            public void run() {
 
                JPanel p1 = new JPanel();
                p1.setBackground(Color.blue);
                p1.setPreferredSize(new Dimension(800, 1600));
                JPanel p2 = new JPanel();
                p2.setBackground(Color.red);
                p2.setPreferredSize(new Dimension(400, 1600));
                JScrollPane jsp1 = new JScrollPane(p1);
                jsp1.setPreferredSize(new Dimension(400, 400));
                p2.add(jsp1);
                p2.setBorder(BorderFactory.createEmptyBorder(60, 60, 60, 60));
                JScrollPane jsp2 = new JScrollPane(p2);
                jsp2.setBorder(BorderFactory.createEmptyBorder(60, 60, 60, 60));
                jsp2.setSize(1000, 1000);
                jsp2.doLayout();
                jsp2.validate();
 
                JWindow f = new JWindow();
                f.getContentPane().add(jsp2);
                f.pack();
 
                f.doLayout();
                f.validate();
 
                BufferedImage im = new BufferedImage(p1.getSize().width, p1
                        .getSize().height, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2d = im.createGraphics();
                f.getContentPane().paint(g2d);
                g2d.dispose();
                f.dispose();
 
                try {
                    ImageIO.write(im, "png", new File("testcomp.png"));
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
 
            }
 
        });
 
    }
 
}