package codingweek2016;

import java.util.ArrayList;

public class RequeteBdd {

	String sqlreq = "";
	String sqlinsert = "INSERT OR IGNORE INTO ";
	String val = " VALUES ( '";
	String v1 = " , ";
	String v2 = "' , '";
	String sqlsearch = "SELECT ";
	String selection = "*";
	String tables = " FROM ( ";
	String tablenoms = "video";
	String cond = " ) WHERE ( ";
	String condand = " ) AND ( ";
	String condor = " ) OR ( ";
	String order = " ) ORDER BY ";// ) avant le order by?
	String desc = " DESC ";
	String p1 = " ('";
	String p2 = "')";
	String p3 = " ('%";
	String p4 = "%')";
	ArrayList<String> attr = new ArrayList<String>();

	public String RequeteSearchBdd(ArrayList<String> req) {

		return RequeteSearchVideo(req);

	}

	public String RequeteSearchVideo(ArrayList<String> req) {
		this.attr.add(0, "videoId");
		this.attr.add(1, "videoTitre");
		this.attr.add(2, "auteurName");
		this.tablenoms = "video";
		sqlreq = sqlsearch + selection + tables + tablenoms + cond;
		
		if (attr.size()>1) {
			sqlreq += "(";

		}
		for (int i = 0; i < attr.size(); i++) {
			
			for (int j = 0; j < req.size(); j++) {
				if (i==0) {
					if (j == 0) sqlreq += attr.get(i) + " = " + p1 + req.get(j) + p2;
					else if (j > 0) {
						sqlreq += condor + attr.get(i) + " LIKE " + p3 + req.get(j) + p4;
					}
				} else if (i>0) {
					sqlreq += condor + attr.get(i) + " LIKE " + p3 + req.get(j) + p4;
				}
			}
		}
		sqlreq += ")";
		sqlreq += order + "nbVues" + desc;
		System.out.println(sqlreq);
		return (sqlreq + ";");
	}
	
	
	
	public String InsertVideo(VOD vod){
		
		
		
		
		sqlreq = sqlinsert + tablenoms + "( ";
		
		for (int i = 0; i<attr.size()-1; i++){
			sqlreq += attr.get(i) + v1;
		}
		sqlreq += attr.get(attr.size()) + " )" + val;
		sqlreq += vod.getId() + v2 + vod.getTitre() + v2 + vod.getAuteur() + v2 + vod.getUrl() + v2 + vod.getNbVues() ;
		
		
		return sqlreq;
		
	}
	
	
}