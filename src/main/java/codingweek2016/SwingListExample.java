package codingweek2016;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

public class SwingListExample extends JPanel {

  private BookEntry books[] = {
      new BookEntry("Ant: The Definitive Guide", "./src/main/java/codingweek2016/1.gif","inutile", new VOD()),
      new BookEntry("Database Programming with JDBC and Java",
          "2.gif","inutile", new VOD()),
      new BookEntry("Developing Java Beans", "3.gif","inutile", new VOD()),
      new BookEntry("Developing JSP Custom Tag Libraries",
          "4.gif","inutile", new VOD()),
      new BookEntry("Java 2D Graphics", "4.gif","inutile", new VOD()),
      new BookEntry("Java and XML", "5.gif","inutile", new VOD()),
      new BookEntry("Java and XSLT", "1.gif","inutile", new VOD()),
      new BookEntry("Java and SOAP", "2.gif","inutile", new VOD()),
      new BookEntry("Learning Java", "3.gif","inutile", new VOD()) };

  private JList booklist = new JList(books);

  public SwingListExample() {
    setLayout(new BorderLayout());
    JButton button = new JButton("Print");
    button.addActionListener(new PrintListener());

    booklist = new JList(books);
    booklist.setCellRenderer(new BookCellRenderer());
    booklist.setVisibleRowCount(4);
    JScrollPane pane = new JScrollPane(booklist);

    add(pane, BorderLayout.NORTH);
    add(button, BorderLayout.SOUTH);
  }

 /* public static void main(String s[]) {
    JFrame frame = new JFrame("List Example");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    JLabel image = new JLabel();
    URL u;
    try { u = new URL("https://i.ytimg.com/vi/DDjcqu9uvUE/default.jpg");
	ImageIcon imageTemp = new ImageIcon(u);
	image.setIcon(imageTemp);
	}
	catch (MalformedURLException e1) {
		e1.printStackTrace();
	}
    
    frame.setContentPane(image);
    frame.pack();
    frame.setVisible(true);
  }*/

  // An inner class to respond to clicks on the Print button
  class PrintListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      int selected[] = booklist.getSelectedIndices();
      System.out.println("Selected Elements:  ");

      for (int i = 0; i < selected.length; i++) {
        BookEntry element = (BookEntry) booklist.getModel()
            .getElementAt(selected[i]);
        System.out.println("  " + element.getTitle());
      }
    }
  }
}

class BookEntry {
  private final String title;

  private final String imagePath;
  
  private final String id;

  private ImageIcon image;
  
  private VOD video;

  public BookEntry(String title, String imagePath, String id, VOD video) {
    this.title = title;
    this.imagePath = imagePath;
    this.id=id;
    this.video=video;
  }

  public String getTitle() {
    return title;
  }

  public ImageIcon getImage() {
    URL u;
    try { u = new URL("https://i.ytimg.com/vi/"+id+"/default.jpg");
		image = new ImageIcon(u);
	}
	catch (MalformedURLException e1) {
		e1.printStackTrace();
	}
    return image;
  }
  
  public String getId(){
	  return this.id;
  }
  
  public VOD getVOD(){
	  return this.video;
  }

  // Override standard toString method to give a useful result
  public String toString() {
    return title;
  }
}

class BookCellRenderer extends JLabel implements ListCellRenderer {
  private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

  public BookCellRenderer() {
    setOpaque(true);
    setIconTextGap(12);
  }

  public Component getListCellRendererComponent(JList list, Object value,
      int index, boolean isSelected, boolean cellHasFocus) {
    BookEntry entry = (BookEntry) value;
    setText(entry.getTitle());
    setIcon(entry.getImage());
    if (isSelected) {
      setBackground(HIGHLIGHT_COLOR);
      setForeground(Color.white);
    } else {
      setBackground(Color.white);
      setForeground(Color.black);
    }
    return this;
  }
}