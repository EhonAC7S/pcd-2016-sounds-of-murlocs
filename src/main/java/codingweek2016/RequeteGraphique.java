package codingweek2016;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.common.collect.Lists;

public class RequeteGraphique {
	
	// nom du fichier avec la clé id
	private static final String PROPERTIES_FILENAME = "youtube.properties";
	// nombre par défaut de videos
	private static final long NUMBER_OF_VIDEOS_RETURNED = 25; 
	// on instancie youtube^^(pour faire nos recherches)
	private static YouTube youtube;
	
	private String motCles;
	private Long nbRes;
	
	private List<VOD> resultatsRecherche;
	
	public RequeteGraphique(String motCles, long nbRes){
		
		if (motCles.length() < 1) {
			// murloc par défaut
			motCles = "murloc";
		} 
		this.motCles=motCles;
		
		if (nbRes < 1){
			//25 par défaut
			nbRes=NUMBER_OF_VIDEOS_RETURNED;
		}
		this.nbRes=nbRes;
		
		this.resultatsRecherche=new ArrayList<VOD>();
		
		// Lis la clé de développeur du fichier de propriétés.
				Properties proprietes = new Properties();

				try {
					InputStream in = RequeteYoutube.class.getResourceAsStream("/ids/" + PROPERTIES_FILENAME);
					proprietes.load(in);

				} catch (IOException e) {
					System.err.println("Erreur de lecture en lisant : " + PROPERTIES_FILENAME + ": " + e.getCause() + " : "
							+ e.getMessage());
					System.exit(1);
				}

				//creation liste pour les permissions/authentification
				List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");

				try {

					// Authorisation de la requête
					Credential credential = Auth.authorize(scopes, "updatevideo");

					youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
							.setApplicationName("Sounds of Murloc").build();

					// cree la liste de resultats de recherche
					YouTube.Search.List recherche = youtube.search().list("id,snippet");

					String cleApi = proprietes.getProperty("youtube.apikey");
					recherche.setKey(cleApi);
					recherche.setQ(this.motCles);

					// restreindre aux videos
					recherche.setType("video");

					recherche.setMaxResults(this.nbRes);

					// affiche resultats
					SearchListResponse resultats = recherche.execute();
					List<SearchResult> resultatsListe = resultats.getItems();
					if (resultatsListe != null) {
						creerVODs(resultatsListe.iterator(), this.resultatsRecherche);
					}
				} catch (GoogleJsonResponseException e) {
					System.err.println("Erreur de service : " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
				} catch (IOException e) {
					System.err.println("Erreur IO : " + e.getCause() + " : " + e.getMessage());
				} catch (Throwable t) {
					t.printStackTrace();
				}
			
	}
	
	private static void creerVODs(Iterator<SearchResult> iteratorResultatsListe,List<VOD> resultatsRecherche){
		int i=0;
		
		if (!iteratorResultatsListe.hasNext()) {
			System.out.println("	Il n'y a aucun résultat pour votre recherche.");
		}

		while (iteratorResultatsListe.hasNext()) {
			SearchResult video = iteratorResultatsListe.next();
			ResourceId rId = video.getId();

			YouTube.Videos.List listVideosRequest;
			try {
				// rajouter les champs voulus dans list("")
				listVideosRequest = youtube.videos().list("snippet,contentDetails,statistics").setId(rId.getVideoId());
				VideoListResponse listResponse = listVideosRequest.execute();
				List<Video> videoList = listResponse.getItems();
				Video test = videoList.get(0);

				// verification que c'est bien une video contenue
				if (rId.getKind().equals("youtube#video")) {

					//recuperation des infos d'une video
					String url = "https://www.youtube.com/v/" + rId.getVideoId() + "?fs=1";
					String titre = video.getSnippet().getTitle() ;
					String auteur = video.getSnippet().getChannelTitle();
					String tags = ""+test.getSnippet().getTags();
					String stats = ""+test.getStatistics().getLikeCount() ;
					String duree = test.getContentDetails().getDuration();
					duree = duree.substring(2, duree.length());
					String lien_image = test.getSnippet().getThumbnails().getDefault().getUrl() ;
					
					String id = rId.getVideoId();
					
					BigInteger nbVues = test.getStatistics().getViewCount();
					
					//creation objet video"info"
					VOD VODRes=new VOD(url,titre,auteur,tags,stats,duree,lien_image, id, nbVues);
					
					//ajout à la liste
					resultatsRecherche.add(i, VODRes);
					
					i++;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	public List<VOD> getResultatsRecherche() {
		return this.resultatsRecherche;
	}
	
}
