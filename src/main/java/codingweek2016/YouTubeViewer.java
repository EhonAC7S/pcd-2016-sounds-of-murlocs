package codingweek2016;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import codingweek2016.BookEntry;
import codingweek2016.VOD;
import com.github.axet.vget.Telechargement;

public class YouTubeViewer {
	
	private static int nbFav=0;
	private static List<VOD> listFav = new ArrayList<VOD>();
	private static List<String> listFav_supp = new ArrayList<String>();
	
	private static List<ArrayList<String>> listFavBDD = new ArrayList<ArrayList<String>>();
	
	
	private static boolean video = false;
	private static boolean list_result = false;
	
	private static JTextField jtf2 = new JTextField("Murloc");
	private static JLabel label2 = new JLabel("Recherche Online: ");
	private static JButton b2 = new JButton ();

	private static JTextField jtf_fav = new JTextField("Murloc");
	private static JLabel label_fav = new JLabel("Recherche dans les favoris : ");
	private static JButton b_fav = new JButton ();
	
	private static JButton b3 = new JButton ();
	
	
	private static JPanel panel_DL = new JPanel();
	private static JScrollPane pane;
	private static JButton button_add;
	private static JButton button_supp;
	private static JButton play;
	private static JPanel left;
	
	private static JFrame frame;
	
	private static RequeteGraphique reqGraph;
	
	private static List<BookEntry> books = new ArrayList<BookEntry>();
	
	private static JList booklist;
	
	private static DefaultListModel listModel = new DefaultListModel();
	
	private static final int NB_RES = 25;//50 max et 0 min(en fait 1 sinon crash^^)
	
		
public static void start() {
	int i;	
	for(i=0;i<NB_RES;i++){
		books.add(i, new BookEntry(".", ".", ".", new VOD())) ;
		listModel.addElement(books.get(i));
	}
	booklist = new JList(listModel);
	BasedeDonnees.Connexion();
	BasedeDonnees.ImportTables();
	
    NativeInterface.open();
    try {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sound/murlocstartapp.wav").getAbsoluteFile());
        Clip clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        clip.start();
    } catch(Exception ex) {
        System.out.println("Error with playing sound.");
        ex.printStackTrace();
    }
    
    SwingUtilities.invokeLater(new Runnable() {
        public void run() {
        	
            try {
            	b2.setIcon(new ImageIcon(ImageIO.read(new File("img/go.png"))));
            	b2.setBorderPainted(false);
            	b2.setBorder(null);
            	b2.setPreferredSize(new Dimension(39, 27));
            } catch (Exception ex) {
              System.out.println(ex);
            }
 //           b2.addActionListener(new BoutonListener3());
        	b2.addActionListener(new BoutonListener2());
        	
        	jtf2.addActionListener(new BoutonListener2());
        	
        	try {
        		b3.setIcon(new ImageIcon(ImageIO.read(new File("img/favorites.png"))));
        		b3.setBorderPainted(false);
        		b3.setBorder(null);
        		b3.setPreferredSize(new Dimension(99, 27));
            } catch (Exception ex) {
              System.out.println(ex);
            }
  //      	b3.addActionListener(new BoutonListener3());
        	b3.addActionListener(new BoutonListener5());
        	
        	try {
        		b_fav.setIcon(new ImageIcon(ImageIO.read(new File("img/go.png"))));
        		b_fav.setBorderPainted(false);
        		b_fav.setBorder(null);
        		b_fav.setPreferredSize(new Dimension(39, 27));
            } catch (Exception ex) {
              System.out.println(ex);
            }
  //  		b_fav.addActionListener(new BoutonListener3());
        	b_fav.addActionListener(new BoutonListener_FavOnline());
        	
        	jtf_fav.addActionListener(new BoutonListener_FavOnline());
        	
        	jtf2.setPreferredSize(new Dimension(90, 20));
        	jtf_fav.setPreferredSize(new Dimension(90, 20));

            frame = new JFrame("MurlocSoundLikeMaster");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            ImageIcon image = new ImageIcon("img/icon1.ico");
            frame.setIconImage(image.getImage());
            
            JPanel top = new JPanel();
            top.add(label2);
            top.add(jtf2);
            top.add(b2);
            top.add(Box.createHorizontalStrut(30));
            top.add(label_fav);
            top.add(jtf_fav);
            top.add(b_fav);
            top.add(b3);
            top.setPreferredSize(new Dimension(820, 40));
           
            
            JPanel top2 = new JPanel();
       
            
        	JLabel label_offline = new JLabel("Accès aux vidéos téléchargées : ");
        	JButton b_offline_search = new JButton ("AFFICHER LE DOSSIER");  
        	
        	b_offline_search.addActionListener(new BoutonListener7());
        	
        	top2.add(label_offline);
        	top2.add(b_offline_search);
            top2.setPreferredSize(new Dimension(850, 40));
            
            frame.getContentPane().add(new Onglets(top,top2), BorderLayout.NORTH);
            
            play = new JButton();
            
            try {
            	play.setIcon(new ImageIcon(ImageIO.read(new File("img/playpng.png"))));
            	play.setBorderPainted(false);
            	play.setBorder(null);
            	play.setPreferredSize(new Dimension(57, 30));
            } catch (Exception ex) {
              System.out.println(ex);
            }
            
        	play.addActionListener(new BoutonListener3());

            button_add = new JButton();
            try {
            	button_add.setIcon(new ImageIcon(ImageIO.read(new File("img/plus.png"))));
            	button_add.setBorderPainted(false);
            	button_add.setBorder(null);
            	button_add.setPreferredSize(new Dimension(43, 30));
            } catch (Exception ex) {
              System.out.println(ex);
            }
  //      	button_add.addActionListener(new BoutonListener3());
        	
            button_add.addActionListener(new BoutonListener4());
            button_add.setPreferredSize(new Dimension(32, 30));
            
            
            button_supp = new JButton();
            try {
            	button_supp.setIcon(new ImageIcon(ImageIO.read(new File("img/moins.png"))));
            	button_supp.setBorderPainted(false);
            	button_supp.setBorder(null);
            	button_supp.setPreferredSize(new Dimension(43, 30));
            } catch (Exception ex) {
              System.out.println(ex);
            }
  //      	button_supp.addActionListener(new BoutonListener3());
            
            button_supp.addActionListener(new BoutonListener_SuppFav());
            button_supp.setPreferredSize(new Dimension(32, 30));
                            
            
            booklist.setCellRenderer(new BookCellRenderer());
            booklist.setFixedCellHeight(100);
            
            pane = new JScrollPane(booklist);
            pane.setPreferredSize(new Dimension(300, 600));
    

            JButton button_DL = new JButton("DOWNLOAD");
            button_DL.addActionListener(new BoutonListener6());
            button_DL.setPreferredSize(new Dimension(280, 20));
            
            panel_DL.add(button_DL);

         
            frame.setSize(930, 650);
            frame.setLocationByPlatform(true);
            frame.setVisible(true);
        }
        
                  
        class BoutonListener2 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
            	
            	if(list_result){
            		frame.remove(((BorderLayout)frame.getContentPane().getLayout()).getLayoutComponent(BorderLayout.WEST));
            	}
            		
            	left = new JPanel();              
                left.setLayout(new BorderLayout());
                
                JPanel buttons = new JPanel();
                buttons.add(play);
                buttons.add(button_add);
                
                left.add(pane, BorderLayout.CENTER);
                left.add(buttons, BorderLayout.NORTH);
                //left.add(panel_DL, BorderLayout.SOUTH);
            	
            	
            	frame.getContentPane().add(left, BorderLayout.WEST);
            	list_result = true;
            		
            	
            	String motCles = jtf2.getText();
            	reqGraph = new RequeteGraphique(motCles, NB_RES);
            	listModel.clear();//attention correction
            	int taille = reqGraph.getResultatsRecherche().size();
            	for (int i = 0; i < taille; i++) {
            		String titre=reqGraph.getResultatsRecherche().get(i).getTitre();
            		String auteur=reqGraph.getResultatsRecherche().get(i).getAuteur();
            		String id=reqGraph.getResultatsRecherche().get(i).getId();
            		String duree=reqGraph.getResultatsRecherche().get(i).getDurée();
            		
            		String time=duree.replaceAll("M"," min ");
            		String time2=time.replaceAll("S"," sec");
            		
            		books.set(i, new BookEntry("<html>"+titre+"<br><i>"+auteur+"</i><br>"+time2+"</html>", "./img/1.gif",id,reqGraph.getResultatsRecherche().get(i)));
   
            		listModel.addElement(books.get(i));//attention correction
				}
            	
            	booklist.setModel(listModel);
            	//Permet l'affichage à jour des textes de recherches
            	SwingUtilities.updateComponentTreeUI(frame);
            	
            }
          }
        
        class BoutonListener3 implements ActionListener {
            public void actionPerformed(ActionEvent e) {
              int selected[] = booklist.getSelectedIndices();
              String id="qMPpnCvCZvw";
              String ids[] = new String[selected.length];
              
              if(video){
            		frame.remove(((BorderLayout)frame.getContentPane().getLayout()).getLayoutComponent(BorderLayout.CENTER));
            	}
              
              if (selected.length==1){
            	  BookEntry element = (BookEntry) booklist.getModel().getElementAt(selected[0]);
            	  id = element.getId();
            	  frame.getContentPane().add(getBrowserPanel(id), BorderLayout.CENTER);
            	  video = true;
              }
              
              else{
            	  
            	  for (int i = 0; i < selected.length; i++) {
            		  BookEntry element = (BookEntry) booklist.getModel().getElementAt(selected[i]);
            		  ids[i]=(element.getId());            		                
            	  }
            	  frame.getContentPane().add(getBrowserPanel(ids),BorderLayout.CENTER);
            	  video = true;
              }
          	
          	frame.setVisible(true);
            }
          }
        
        class BoutonListener4 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
            	//System.out.println("Bouton ADD en cours de construction ! :D");
            	
            	int selected[] = booklist.getSelectedIndices();
                
                for (int i = 0; i < selected.length; i++) {
                  BookEntry element = (BookEntry) booklist.getModel()
                      .getElementAt(selected[i]);
                  
                  System.out.println("Element "+i+"e :"+element.getVOD());
                  
                  listFav.add(nbFav,element.getVOD());
                  nbFav++;
                }
                
                for (int i = 0; i < nbFav; i++) { 
                	//listFav.get(i).affiche();
                	//System.out.println("coucou");
                	
                	System.out.println("ID de la vidéo à ajouter : "+listFav.get(i).getId());
                	
                    BasedeDonnees.EnregVid(listFav.get(i));//des caractères spéciaux (comme ' ) font crash...
                    //System.out.println("coucoubis");
                  }
                for (int i = 0; i < nbFav; i++) { 
                    listFav.remove(i);
                  }
                nbFav=0;
            }
          }
        
        class BoutonListener5 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
            	
                if(list_result){
            		frame.remove(((BorderLayout)frame.getContentPane().getLayout()).getLayoutComponent(BorderLayout.WEST));
            	}
            		
                left = new JPanel();              
                left.setLayout(new BorderLayout());
                
                JPanel buttons = new JPanel();
                buttons.add(play);
                buttons.add(button_supp);
                
                left.add(pane, BorderLayout.CENTER);
                left.add(buttons, BorderLayout.NORTH);
                left.add(panel_DL, BorderLayout.SOUTH);
                
                
            	frame.getContentPane().add(left, BorderLayout.WEST);
            	list_result = true;
            		
            	listFavBDD=BasedeDonnees.AfficherFavoris();//affiche en console aussi
            	
            	   
            	listModel.clear(); 
            	int taille = listFavBDD.size();
            	for (int i = 0; i < taille; i++) {
            		String titre=listFavBDD.get(i).get(1);
            		String auteur=listFavBDD.get(i).get(2);
            		String id=listFavBDD.get(i).get(0);
            		
            		books.set(i, new BookEntry("<html>"+titre+"<br><i>"+auteur+"</html>", "./img/1.gif",id,new VOD()));
            		listModel.addElement(books.get(i));
				}
            	
            	booklist.setModel(listModel);
            	//Permet l'affichage à jour des textes de recherches
            	SwingUtilities.updateComponentTreeUI(frame);
                

            }
          }
        
        class BoutonListener6 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
            	//int selected[] = booklist.getSelectedIndices();
                final int selected[] = booklist.getSelectedIndices();
                Thread thread = new Thread() {
                	public void run() {
                		for (int i = 0; i < selected.length; i++) {
                		BookEntry element = (BookEntry) booklist.getModel().getElementAt(selected[i]);
                		Telechargement dl = new Telechargement(element.getId(),element.getTitle());
                		//TODO : FAUT IL AJOUTER CETTE STRUCTURE TELECHARGEMENT A LA BD CAR ELLE RESSMBLE LES URL LOCALES DES FICHIERS AUDIOS VIDEO?
                		}
                	}
                };
                thread.start();
            }
          }
        
        class BoutonListener7 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
  
            	String localPath = System.getProperty("user.dir" );
            	char separator = File.separatorChar;
            	File dir = new File(localPath + separator + "DL" + separator);
            	if (Desktop.isDesktopSupported()) {
            	    try {
						Desktop.getDesktop().open(dir);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
            	}

            }
          }
        
        class BoutonListener8 implements ActionListener{
            public void actionPerformed(ActionEvent e) {
  
            	System.out.println("Bouton Mes Favoris offline en cours de construction ! :D");

            }
          }
        
        class BoutonListener_FavOnline implements ActionListener{
        	public void actionPerformed(ActionEvent e) {
            	
            	if(list_result){
            		frame.remove(((BorderLayout)frame.getContentPane().getLayout()).getLayoutComponent(BorderLayout.WEST));
            	}
            		
            	left = new JPanel();              
                left.setLayout(new BorderLayout());
                
                JPanel buttons = new JPanel();
                buttons.add(play);
                buttons.add(button_supp);
                
                left.add(pane, BorderLayout.CENTER);
                left.add(buttons, BorderLayout.NORTH);
                left.add(panel_DL, BorderLayout.SOUTH);
                
            	
            	frame.getContentPane().add(left, BorderLayout.WEST);
            	list_result = true;
            		
            	
            	String motCles = jtf_fav.getText();
            	
            	listFavBDD=BasedeDonnees.RechercheVideo(motCles);//affiche en console aussi
            	
          	   
            	listModel.clear(); 
            	int taille = listFavBDD.size();
            	
            	if (taille==0){
            		listModel.addElement(new BookEntry("", "", "", new VOD()));
            	}
            	
            	for (int i = 0; i < taille; i++) {
            		String titre=listFavBDD.get(i).get(1);
            		String auteur=listFavBDD.get(i).get(2);
            		String id=listFavBDD.get(i).get(0);
            		
            		books.set(i, new BookEntry("<html>"+titre+"<br><i>"+auteur+"</html>", "./img/1.gif",id,new VOD()));
            		listModel.addElement(books.get(i));
				}
            	
            	booklist.setModel(listModel);
            	
            	
            	//Permet l'affichage Ã  jour des textes de recherches #yolo
            	SwingUtilities.updateComponentTreeUI(frame);
            	
            }
          }
        
        class BoutonListener_SuppFav implements ActionListener{
            public void actionPerformed(ActionEvent e) {
  
            	int selected[] = booklist.getSelectedIndices();           	
                nbFav=0;
            	
                for (int i = 0; i < selected.length; i++) {
                  BookEntry element = (BookEntry) booklist.getModel()
                      .getElementAt(selected[i]);
                  
                  listFav_supp.add(nbFav,element.getId());
                  nbFav++;
                }
                
                for (int i = 0; i < nbFav; i++) { 
                	//listFav.get(i).affiche();
                	//System.out.println("coucou"););
                	
                    BasedeDonnees.DesenregVid(listFav_supp.get(i));//des caractères spéciaux (comme ' ) font crash...
                    //System.out.println("coucoubis");
                  }
                for (int i = 0; i < nbFav; i++) { 
                    listFav_supp.remove(i);
                  }
                nbFav=0;
                
                
                if(list_result){
            		frame.remove(((BorderLayout)frame.getContentPane().getLayout()).getLayoutComponent(BorderLayout.WEST));
            	}
            		
                left = new JPanel();              
                left.setLayout(new BorderLayout());
                
                JPanel buttons = new JPanel();
                buttons.add(play);
                buttons.add(button_supp);
                
                left.add(pane, BorderLayout.CENTER);
                left.add(buttons, BorderLayout.NORTH);
                left.add(panel_DL, BorderLayout.SOUTH);
                
                
            	frame.getContentPane().add(left, BorderLayout.WEST);
            	list_result = true;
            		
            	listFavBDD=BasedeDonnees.AfficherFavoris();//affiche en console aussi
            	
            	   
            	listModel.clear(); 
            	int taille = listFavBDD.size();
            	for (int i = 0; i < taille; i++) {
            		String titre=listFavBDD.get(i).get(1);
            		String auteur=listFavBDD.get(i).get(2);
            		String id=listFavBDD.get(i).get(0);
            		
            		books.set(i, new BookEntry("<html>"+titre+"<br><i>"+auteur+"</html>", "./img/1.gif",id,new VOD()));
            		listModel.addElement(books.get(i));
				}
            	
            	booklist.setModel(listModel);
            	//Permet l'affichage à jour des textes de recherches
            	SwingUtilities.updateComponentTreeUI(frame);
            }
          }
        
    });
    NativeInterface.runEventPump();
    // don't forget to properly close native components
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        @Override
        public void run() {
            NativeInterface.close();
        }
    }));
}

public static JPanel getBrowserPanel(String id) {
    JPanel webBrowserPanel = new JPanel(new BorderLayout());
    JWebBrowser webBrowser = new JWebBrowser();
    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
    webBrowser.setBarsVisible(false);
    //webBrowser.navigate("https://www.youtube.com/v/b-Cr0EWwaTk?fs=1");
    webBrowser.navigate("https://www.youtube.com/v/" + id + "?fs=1");
    return webBrowserPanel;
}

public static JPanel getBrowserPanel(String[] tab_id) {
    JPanel webBrowserPanel = new JPanel(new BorderLayout());
    JWebBrowser webBrowser = new JWebBrowser();
    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
    webBrowser.setBarsVisible(false);
    //webBrowser.navigate("https://www.youtube.com/v/b-Cr0EWwaTk?fs=1");
    
    String temp = tab_id[0];
    for(int i=1;i<tab_id.length;i++){
    	temp = temp + "," + tab_id[i];
    }
    
    webBrowser.navigate("https://www.youtube.com/embed/VIDEO_ID?playlist=" + temp);
    return webBrowserPanel;
}

}
