package codingweek2016;

import java.math.BigInteger;

public class VOD {
	
	//infos relatives à une video
	private String url;
	private String titre;
	private String auteur;
	private String tags;
	private String stats;
	private String duree;
	private String lien_image;
	
	private String id;
	
	private BigInteger nbVues;
	
	public VOD(){
		this.url = "";
		this.titre = "";
		this.auteur = "";
		this.tags = "";
		this.stats = "";
		this.duree = "";
		this.lien_image = "";
		
		this.id="";
		
		this.nbVues=BigInteger.ZERO;
	}
	
	public VOD(String url, String titre, String auteur, String tags, String stats, String duree, String lien_image, String id, BigInteger nbVues) {
		this.url = url;
		this.titre = titre;
		this.auteur = auteur;
		this.tags = tags;
		this.stats = stats;
		this.duree = duree;
		this.lien_image = lien_image;
		this.id=id;
		this.nbVues=nbVues;
	}
	
	public String getUrl() {
		return this.url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitre() {
		return this.titre;
	}
	public String getTitle() {
		String title = "";
		String[] decoupe = this.titre.replace("'", "''").split("");
		for (String myString : decoupe) {
			title += myString;
			}

		return title;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getAuthor() {
		String author = "";
		String[] decoupe = this.auteur.replace("'", "''").split("");
		for (String myString : decoupe) {
			author += myString;
			}

		return author;
	}
	public String getAuteur() {
		return this.auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getTags() {
		return this.tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getStats() {
		return this.stats;
	}
	public void setStats(String stats) {
		this.stats = stats;
	}
	public String getDurée() {
		this.duree.replace("H", ":").replace("M", ":").replace("S", "");
		return this.duree;
	}
	public void setDurée(String duree) {
		this.duree = duree;
	}
	public String getLien_image() {
		return this.lien_image;
	}
	public void setLien_image(String lien_image) {
		this.lien_image = lien_image;
	}
	
	//pour verifier que ça marche bien
	public void affiche(){
		System.out.println("URL : " + this.url);
		System.out.println("Titre : " + this.titre);
		System.out.println("Auteur : " + this.auteur);
		System.out.println("Liste des tags : " + this.tags);
		System.out.println("Statistiques : " + this.stats + " pouces bleus");
		System.out.println("Durée : " + this.duree);
		System.out.println("Lien image : " + this.lien_image);
		System.out.println("ID : " + this.id);
		System.out.println("Nombre de Vues : " + this.nbVues);
		System.out.println("\n");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigInteger getNbVues() {
		return this.nbVues;
	}

	public void setNbVues(BigInteger nbVues) {
		this.nbVues = nbVues;
	}
	
	
}
