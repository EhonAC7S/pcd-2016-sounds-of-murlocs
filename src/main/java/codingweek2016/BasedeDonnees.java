package codingweek2016;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;

//import com.mysql.jdbc.ResultSetMetaData;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


import javafx.scene.media.MediaPlayer;

import javax.print.attribute.standard.Media;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class BasedeDonnees {
	//static BasedeDonnees bdd = new BasedeDonnees();

	static Connection c = null;
	static String dbnom = "test.db";
	static String query;
	
	public static void main(String args[]) {
		

		Connexion();
		ImportTables();
		RechercheVideo(query);
		//AfficherFavoris();
		Deconnexion();

	}

	public static void Connexion() {
		try {
			
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully");

	}

	public static void ImportTables() {
		try {
			Statement stmt = null;
			stmt = c.createStatement();
			
			String sqlCreateTableVideo = "CREATE TABLE IF NOT EXISTS VIDEO "
					+ "(videoId           TEXT PRIMARY KEY     NOT NULL, "
					+ " videoTitre        TEXT                                  NOT NULL, "
					+ " auteurName        TEXT, " 
//					+ " description       TEXT, "
//					+ " videoLength       TEXT, " 
					+ " videoURL          TEXT                      NOT NULL, "
					+ " nbVues            INTEGER)";

			stmt.executeUpdate(sqlCreateTableVideo);

			String sqlCreateTablePlaylist = "CREATE TABLE IF NOT EXISTS PLAYLIST "
					+ "(playlistId        INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL, "
					+ " playlistName      TEXT                      NOT NULL, "
					+ " auteurName        TEXT                      NOT NULL, " 
//					+ " nbrVideo          INTEGER, "
//					+ " playlistDesc      TEXT, " 
//					+ " creationDate      TEXT                      NOT NULL, "
//					+ " updateDate        TEXT, " 
					+ " playlistURL       TEXT                      NOT NULL, "
					+ " nbrVues           INTEGER)";

			stmt.executeUpdate(sqlCreateTablePlaylist);

			String sqlCreateTablePlaylistVid = "CREATE TABLE IF NOT EXISTS PLAYLISTVID "
					+ "(playlistVidId        INTEGER PRIMARY KEY AUTOINCREMENT      NOT NULL, "
					+ " playlistId           INTEGER, "
					+ " videoId              INTEGER, "
//					+ " auteurName           TEXT       NOT NULL, "
//					+ " numberList           INTEGER, "
					+ " FOREIGN KEY (playlistId)           REFERENCES PLAYLIST(playlistId), "
					+ " FOREIGN KEY (videoId)              REFERENCES VIDEO(videoId)) ";

			stmt.executeUpdate(sqlCreateTablePlaylistVid);

			String sqlCreateTableVideoTags = "CREATE TABLE IF NOT EXISTS VIDEOTAGS "
					+ "(videoTagId           INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL, "
					+ " videoId              INTEGER, "
					+ " tagName              TEXT, " 
//					+ " coefficientTag       INTEGER, "
					+ " FOREIGN KEY (videoId)              REFERENCES VIDEO(videoId))";

			stmt.executeUpdate(sqlCreateTableVideoTags);

			String sqlRequete = "CREATE TABLE IF NOT EXISTS REQUETE "
					+ "(requeteId            INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL, "
//					+ " auteurName           TEXT       NOT NULL, " 
					+ " requete              TEXT, "
					+ " nbRes                INT)";

			stmt.executeUpdate(sqlRequete);

			stmt.close();
			
/*
			int a = 1234;
			int b = 1123456875;
			stmt = c.createStatement();
			String sqlInsertTableVideo = "INSERT INTO VIDEO(videoId, videoTitre, auteurName, nbVues)"
					+ " VALUES ('"
					+ a + "' , '"
					+ "titre" + "' , '"
					+ "nom auteur" + "' , '"
					+ b + "')";

			stmt.executeUpdate(sqlInsertTableVideo);
			c.close();
			
*/

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Table créées avec succès.");

	}
	
	
	
	public static void EnregVid(VOD vidbdd) {
		//RequeteBdd req = new RequeteBdd();
		
		try {
			Statement stmt = null;
			
			stmt = c.createStatement();
							

			String sqlInsertTableVideo = "INSERT OR IGNORE INTO VIDEO(videoId, videoTitre, auteurName, videoURL, nbVues)"
					+ "VALUES ('"
 					+ vidbdd.getId() + "' , '"
					+ vidbdd.getTitle() + "' , '"
					+ vidbdd.getAuthor() + "' , '"
//					+ " description       TEXT, "
//					+ vidbdd.getDurée() + "' , '"
					+ vidbdd.getUrl() + "' , '"
					+ vidbdd.getNbVues() + "');";

			

			stmt.executeUpdate(sqlInsertTableVideo);
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Video saved succesfully");

	}
	
	
	public static void DesenregVid(String id_vod) {
		//RequeteBdd req = new RequeteBdd();
		
		try {
			Statement stmt = null;
			
			stmt = c.createStatement();
							

			String sqlDeleteTableVideo = "DELETE FROM VIDEO WHERE (videoId = '"
					+ id_vod + "');";
			
			System.out.println(sqlDeleteTableVideo);

			stmt.executeUpdate(sqlDeleteTableVideo);
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Video unsaved succesfully");

	}
	
	
	public static ArrayList<ArrayList<String>> AfficherFavoris() {
		
		ArrayList<ArrayList<String>> arrayVid = new ArrayList<ArrayList<String>>();
		
		try {
			Statement stmt = null;
			
			stmt = c.createStatement();

			String sqlFavorisVideo = "SELECT * FROM VIDEO";

			ResultSet rs = null;
			rs = stmt.executeQuery(sqlFavorisVideo);

			java.sql.ResultSetMetaData rsmd = rs.getMetaData();
			TablePrinter.printTable(c, "video");
			int columnsNumber = rsmd.getColumnCount();
			
			
			int list=0;
			while (rs.next()) {
				if( list >= arrayVid.size())
				      arrayVid.add(new ArrayList());
			    for (int i = 1; i <= columnsNumber; i++) {
			    	if (i == 1) System.out.print(" { ");
			        String columnValue = rs.getString(i);
			        System.out.print(rsmd.getColumnName(i) + " : " + columnValue);
			        arrayVid.get(list).add(columnValue);
			        if (i == columnsNumber) System.out.print(" } ");
			        else System.out.print(" , ");
			    }
			    list++;
			    System.out.println("\n");
			    System.out.println("");
			}
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Sélection des vidéos favorites.");
		


		return arrayVid;
	}
	
	
/*public static ArrayList<ArrayList<String>> RechercherFavoris(String motCles) {
		
		ArrayList<ArrayList<String>> arrayVid = new ArrayList<ArrayList<String>>();
		
		try {
			Statement stmt = null;
			
			stmt = c.createStatement();

			String sqlFavorisVideo = "SELECT * FROM video WHERE videoTitre LIKE '%"+motCles+"%' OR auteurName LIKE '%"+motCles+"%' ;"
					//ajouts à faire encore
					;

			ResultSet rs = null;
			rs = stmt.executeQuery(sqlFavorisVideo);

			java.sql.ResultSetMetaData rsmd = rs.getMetaData();
			TablePrinter.printTable(c, "video");
			int columnsNumber = rsmd.getColumnCount();
			
			
			int list=0;
			while (rs.next()) {
				if( list >= arrayVid.size())
				      arrayVid.add(new ArrayList());
			    for (int i = 1; i <= columnsNumber; i++) {
			    	if (i == 1) System.out.print(" { ");
			        String columnValue = rs.getString(i);
			        System.out.print(rsmd.getColumnName(i) + " : " + columnValue);
			        arrayVid.get(list).add(columnValue);
			        if (i == columnsNumber) System.out.print(" } ");
			        else System.out.print(" , ");
			    }
			    list++;
			    System.out.println("\n");
			    System.out.println("");
			}
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Recherche dand vidéos favorites.");
		


		return arrayVid;
	}
	inutile maintenant*/
	
	
	
	public static ArrayList<ArrayList<String>> RechercheVideo(String query) {
		
		ArrayList<ArrayList<String>> arrayVid = new ArrayList<ArrayList<String>>();

		ContainerSearch searchbdd = new ContainerSearch();
		searchbdd.setText(query);
		searchbdd.getTags();
		TablePrinter.printTable(c, "video");
	
		RequeteBdd req = new RequeteBdd();
		
			
			try {
				Statement stmt = null;
				stmt = c.createStatement();
				
				String sqlRechercheVideo = req.RequeteSearchBdd(searchbdd.tags);
	
				ResultSet rs = stmt.executeQuery(sqlRechercheVideo);

				java.sql.ResultSetMetaData rsmd = rs.getMetaData();
				int columnsNumber = rsmd.getColumnCount();
				
				int list=0;
				while (rs.next()) {
					if( list >= arrayVid.size())
					      arrayVid.add(new ArrayList());
				    for (int i = 1; i <= columnsNumber; i++) {
				    	if (i == 1) System.out.print(" { ");
				        String columnValue = rs.getString(i);
				        System.out.print(rsmd.getColumnName(i) + " : " + columnValue);
				        arrayVid.get(list).add(columnValue);
				        if (i == columnsNumber) System.out.print(" } ");
				        else System.out.print(" , ");
				    }
				    list++;
				    System.out.println("\n");
				    System.out.println("");
				}
				stmt.close();
			} catch (Exception e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(0);
			}
			System.out.println("Recherche dans la base de données terminée.");
			
			return arrayVid;
		}
	
	
	public static void Deconnexion() {
		try {
			c.close();
			System.out.println("Déconnexion réussie.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
